import gj
import gleam/dynamic.{Dynamic}
import gleam/io
import gleam/list
import gleam/result
import gleam/should
import gleam/string

external type CharList

// Tests shamelesly stolen from jason and the json_test_suite:
// https://github.com/michalmuskala/jason/blob/master/test/json_test_suite_test.exs
// https://github.com/nst/JSONTestSuite
pub fn json_test_suite_test() {
  let accept_extra = [
    "i_number_double_huge_neg_exp.json", "i_number_real_underflow.json", "i_number_too_big_neg_int.json",
    "i_number_too_big_pos_int.json", "i_number_very_big_negative_int.json", "i_structure_500_nested_arrays.json",
  ]
  let reject_extra = [
    "i_number_neg_int_huge_exp.json", "i_number_pos_double_huge_exp.json", "i_number_real_neg_overflow.json",
    "i_number_real_pos_overflow.json", "i_object_key_lone_2nd_surrogate.json", "i_string_1st_surrogate_but_2nd_missing.json",
    "i_string_1st_valid_surrogate_2nd_invalid.json", "i_string_UTF-16LE_with_BOM.json",
    "i_string_UTF-8_invalid_sequence.json", "i_string_UTF8_surrogate_U+D800.json",
    "i_string_incomplete_surrogate_and_escape_valid.json", "i_string_incomplete_surrogate_pair.json",
    "i_string_incomplete_surrogates_escape_valid.json", "i_string_invalid_surrogate.json",
    "i_string_invalid_utf-8.json", "i_string_inverted_surrogates_U+1D11E.json", "i_string_iso_latin_1.json",
    "i_string_lone_second_surrogate.json", "i_string_lone_utf8_continuation_byte.json",
    "i_string_not_in_unicode_range.json", "i_string_overlong_sequence_2_bytes.json",
    "i_string_overlong_sequence_6_bytes.json", "i_string_overlong_sequence_6_bytes_null.json",
    "i_string_truncated-utf-8.json", "i_string_utf16BE_no_BOM.json", "i_string_utf16LE_no_BOM.json",
    "i_structure_UTF-8_BOM_empty_object.json",
  ]
  let dir = "./json_test_suite/"
  assert Ok(files) =
    list_files(dir)
    |> result.map(list.map(_, char_list_to_string))
  let tuple(accept, other) =
    files
    |> list.partition(fn(f) { string.starts_with(f, "y") })
  let tuple(reject, _ignore) =
    other
    |> list.partition(fn(f) { string.starts_with(f, "n") })

  // ACCEPT
  list.map(
    list.append(accept, accept_extra),
    fn(file) {
      assert Ok(json) = read_file(string.concat([dir, file]))
      let res = gj.decode(json)
      case res {
        Ok(_) -> True
        _ -> {
          debug(file)
          debug(res)
          False
        }
      }
    },
  )
  |> list.any(fn(x) { x == False })
  |> should.equal(False)

  // REJECT
  list.map(
    list.append(reject, reject_extra),
    fn(file) {
      assert Ok(json) = read_file(string.concat([dir, file]))
      let res = gj.decode(json)
      case res {
        Error(_) -> True
        Ok(_) -> {
          debug(file)
          debug(res)
          False
        }
      }
    },
  )
  |> list.any(fn(x) { x == False })
  |> should.equal(False)
}

external fn read_file(name: String) -> Result(String, Dynamic) =
  "file" "read_file"

external fn list_files(dir_name: String) -> Result(List(CharList), Dynamic) =
  "file" "list_dir"

external fn debug(msg: a) -> a =
  "test_help" "debug"

external fn char_list_to_string(CharList) -> String =
  "erlang" "list_to_binary"
