-module(test_help).

-export([debug/1]).

% Be able to write to std out during rebar3 eunit
debug(In) ->
  io:format(user, "~tp\n", [In]),
  In.
