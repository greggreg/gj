import generic_json.{Array, Boolean, Null, Number, Object, String}
import gleam/should
import gj

pub fn encode_test() {
  gj.encode(Array([
    Null,
    Boolean(True),
    Boolean(False),
    Array([Array([])]),
    Object([tuple("a", Number(1.0)), tuple("b", Array([]))]),
  ]))
  |> should.equal("[null,true,false,[[]],{\"a\":1.0,\"b\":[]}]")
}
