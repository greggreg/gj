import gj
import gj_decode.{
  InvalidNumber, InvalidString, InvalidUnicodeSequence, ParseError, UnexpectedEndOfInput,
  UnexpectedToken,
}
import generic_json.{Array, Boolean, JSON, Null, Number, Object, String}
import gleam/bit_string
import gleam/io
import gleam/result
import gleam/should

// Tests shamelessly taken from jason: https://github.com/michalmuskala/jason/blob/master/test/decode_test.exs
pub fn numbers_test() {
  assert_fail("-", UnexpectedEndOfInput)
  assert_fail("--1", UnexpectedToken("-"))
  assert_fail("01", UnexpectedToken("1"))
  assert_fail(".1", UnexpectedToken("."))
  assert_fail("1.", UnexpectedEndOfInput)
  assert_fail("1e", UnexpectedEndOfInput)
  assert_fail("1.0e+", UnexpectedEndOfInput)
  assert_fail("1e999", InvalidNumber)
  assert_pass("0", Number(0.0))
  assert_pass("1", Number(1.0))
  assert_pass("-0", Number(0.0))
  assert_pass("-1", Number(-1.0))
  assert_pass("0.1", Number(0.1))
  assert_pass("-0.1", Number(-0.1))
  assert_pass("0e0", Number(0.0))
  assert_pass("0E0", Number(0.0))
  assert_pass("1e0", Number(1.0))
  assert_pass("1E0", Number(1.0))
  assert_pass("1.0e0", Number(1.0))
  assert_pass("1e+0", Number(1.0))
  assert_pass("1.0e+0", Number(1.0))
  assert_pass("0.1e1", Number(1.0))
  assert_pass("0.1e-1", Number(0.01))
  // TODO: Does gleam have sci notation?
  assert_pass(
    "99.99e99",
    Number(
      99990000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0,
    ),
  )
  assert_pass(
    "-99.99e-99",
    Number(
      -0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009999,
    ),
  )
  assert_pass(
    "123456789.123456789e123",
    Number(
      123456789123456789000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0,
    ),
  )
}

pub fn strings_test() {
  assert_fail("\"", UnexpectedEndOfInput)
  assert_fail("\"\\\"", UnexpectedEndOfInput)
  assert_fail("\"\\k\"", UnexpectedToken("k"))
  // TODO: write this test correctly
  // assert_fail(
  //   result.unwrap(bit_string.to_string(<<34:8, 128:16, 34:8>>), ""),
  //   UnexpectedToken("X"),
  // )
  assert_fail("\"\\u2603\\\"", UnexpectedEndOfInput)
  assert_fail(
    "\"Here's a snowman for you: ☃. Good day!",
    UnexpectedEndOfInput,
  )
  assert_fail("\"𝄞", UnexpectedEndOfInput)
  assert_fail("\\u001F)", UnexpectedToken("\\"))
  assert_fail("\"\\ud8aa\\udcxx\"", UnexpectedToken("x"))
  assert_fail("\"\\ud8aa\\uda00\"", InvalidUnicodeSequence)
  assert_fail("\"\\uxxxx\"", UnexpectedToken("x"))
  assert_pass("  \"\\n\\r\\t\\b\\/\\\\\"  ", String("\n\r\t\b\/\\"))
  assert_pass("\"\\u2603\"", String("☃"))
  assert_pass("\"\\uD834\\uDD1E\"", String("𝄞"))
  assert_pass("\"\\uD799\\uD799\"", String("힙힙"))
  assert_pass("\"✔︎\"", String("✔︎"))
}

pub fn objects_test() {
  assert_fail("{", UnexpectedEndOfInput)
  assert_fail("{,", UnexpectedToken(","))
  assert_fail("{\"foo\"}", UnexpectedToken("}"))
  assert_fail("{\"foo\": \"bar\",}", UnexpectedToken("}"))
  assert_pass("{}", Object([]))
  assert_pass("{\"foo\": \"bar\"}", Object([tuple("foo", String("bar"))]))
  assert_pass("{\"foo\"  : \"bar\"}", Object([tuple("foo", String("bar"))]))
  let expected =
    Object([tuple("foo", String("bar")), tuple("baz", String("quux"))])
  assert_pass("{\"foo\": \"bar\", \"baz\": \"quux\"}", expected)
  let expected2 = Object([tuple("foo", Object([tuple("bar", String("baz"))]))])
  assert_pass("{\"foo\": {\"bar\": \"baz\"}}", expected2)
}

pub fn arrays_test() {
  assert_fail("[", UnexpectedEndOfInput)
  assert_fail("[,", UnexpectedToken(","))
  assert_fail("[1,]", UnexpectedToken("]"))
  assert_pass("[]", Array([]))
  assert_pass("[1, 2, 3]", Array([Number(1.0), Number(2.0), Number(3.0)]))
  assert_pass(
    "[\"foo\", \"bar\", \"baz\"]",
    Array([String("foo"), String("bar"), String("baz")]),
  )
  assert_pass(
    "[{\"foo\": \"bar\"}]",
    Array([Object([tuple("foo", String("bar"))])]),
  )
}

pub fn whitespace_test() {
  assert_fail("", UnexpectedEndOfInput)
  assert_fail("    ", UnexpectedEndOfInput)
  assert_pass("  [  ]  ", Array([]))
  assert_pass("  {  }  ", Object([]))
  assert_pass(
    "  [  1  ,  2  ,  3  ]  ",
    Array([Number(1.0), Number(2.0), Number(3.0)]),
  )
  let expected =
    Object([tuple("foo", String("bar")), tuple("baz", String("quux"))])
  assert_pass(
    "  {  \"foo\"  :  \"bar\"  ,  \"baz\"  :  \"quux\"  }  ",
    expected,
  )
}

fn assert_pass(in: String, out: JSON) {
  should.equal(gj.decode(in), Ok(out))
}

fn assert_fail(in: String, out: ParseError) {
  assert Error(tuple(err, _)) = gj.decode(in)
  should.equal(err, out)
}
