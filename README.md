# gj

**[WIP]**

A fast, spec compliant, generic JSON parser and encoder in Gleam

## Documentation
[https://hexdocs.pm/gj/](https://hexdocs.pm/gj/)

## Quick start

in your `rebar.config` deps section add:
```erlang
{deps, [
  gj
]]
```

```rust
import gj

// Decoding
> gj.decode("[1]")
Ok(Array([Number(1.0)]))

// Encoding
> gj.encode(Array([Number(1.0)]))
Ok("[1.0]")
```


