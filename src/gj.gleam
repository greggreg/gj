//// A fast, spec compliant, generic JSON parser and encoder in Gleam
////
////  - Json strings are parsed into the JSON type.
////
////  - All numbers are floats.
////
////  - Adheres to the spec
////
////  - Passes all tests in the json test suite https://github.com/nst/JSONTestSuite
////
//// # Quick Usage
////
////    > gj.decode("[1]")
////    Ok(Array([Number(1.0)]))
////
////    > gj.encode(Array([Number(1.0)]))
////    Ok("[1.0]")

import gj_decode.{ParseError, ParseErrorLocation}
import gj_encode
import generic_json.{JSON}

pub fn decode(
  str: String,
) -> Result(JSON, tuple(ParseError, ParseErrorLocation)) {
  gj_decode.decode(str)
}

pub fn encode(json: JSON) -> String {
  gj_encode.encode(json)
}
