-module(gj_bridge).

-export([next_grapheme/1, uni_to_bin/1, valid_in_string/1]).

% We need to quickly test if a character is allowed to be inserted into a string
valid_in_string(In) ->
    case string:next_codepoint(In) of
      [C | _] when C > 31-> true;
      _ -> false
    end. 

% Gleam's pop_grapheme seems to be broken, this is a workaround
next_grapheme(In) ->
    B = string:next_grapheme(In),
    % erlang:display(B),
    case B of
      [G | []] when is_list(G) ->
          {ok, {unicode:characters_to_binary(G), <<""/utf8>>}};
          
      [G1 | []] ->
          {ok, {<<G1/utf8>>, <<""/utf8>>}};

      [G2 | Rest] when is_list(G2) ->
          {ok, {unicode:characters_to_binary(G2), Rest}};
          
      [G3 | Rest] ->
          {ok, {<<G3/utf8>>, Rest}};

      {error, _} ->
          {error, nil};

      _ ->
          {error, nil}
    end.

% Try to convert utf16 escapes (\uXXXX or \uXXXX\uXXXX) to Gleam string
uni_to_bin(Uni) ->
    % erlang:display(Uni),
    case unicode:characters_to_binary(Uni, utf16, unicode) of
      {incomplete, _, _} -> incomplete;
      X when is_binary(X) -> {complete, X};
      {error, _, _} -> nogood
    end.
      


