/// Type for representing JSON generically
///
/// This is the type that `gj.decode` produces and `gj.encode` takes
pub type JSON {
  Object(List(tuple(String, JSON)))
  Array(List(JSON))
  Number(Float)
  Boolean(Bool)
  String(String)
  Null
}
