//// Encoder module for GJ
////
//// See module gj for the public interface

import generic_json.{Array, Boolean, JSON, Null, Number, Object, String}
import gleam/float
import gleam/list
import gleam/string

// TODO: this is naive and sholud be written better / perf tested
// possibly support options for string encoding
// provide pretty printer
pub fn encode(j: JSON) -> String {
  case j {
    Boolean(True) -> "true"
    Boolean(False) -> "false"
    Null -> "null"
    Array(vals) ->
      [
        "[",
        list.map(vals, encode)
        |> string.join(","),
        "]",
      ]
      |> string.concat()

    Object(vals) ->
      [
        "{",
        list.map(
          vals,
          fn(kv) {
            let tuple(key, val) = kv
            [encode(String(key)), ":", encode(val)]
            |> string.concat()
          },
        )
        |> string.join(","),
        "}",
      ]
      |> string.concat()
    Number(f) -> float.to_string(f)

    // todo, properly encode unicode string
    String(s) ->
      ["\"", s, "\""]
      |> string.concat()
  }
}
